package app.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
	// -----------------------------Oracle------------------------------

	// private String url = "jdbc:oracle:thin:@//127.0.0.1:3306/XE";
	// private String driverClass = "oracle.jdbc.driver.OracleDriver";

	// -----------------------------MariaDB-----------------------------

	private String url = "jdbc:mariadb://127.0.0.1:3306/loginapp";
	private String driverClass = "org.mariadb.jdbc.Driver";

	private String usuario = "usuario";
	private String password = "usuario";

	public Connection getNewConnection() throws SQLException {

		try {
			Class.forName(driverClass);
		} catch (ClassNotFoundException e) {
			System.err.println("No se encuentra el driver JDBC. Revisa su configuracin");
			throw new RuntimeException(e);
		}

		Connection conn = DriverManager.getConnection(url, usuario, password);
		return conn;
	}
}
