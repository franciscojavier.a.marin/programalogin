package app.service;

import java.sql.Connection;
import java.sql.SQLException;

import app.dao.UsuarioDao;
import app.excepciones.DataException;
import app.excepciones.LoginException;
import app.modelo.Usuario;

public class UsuarioService {
	private ConnectionProvider conn;
	private UsuarioDao dao;

	public UsuarioService() {
		conn = new ConnectionProvider();
		dao = new UsuarioDao();
	}

	/*-----------------------------------Login-----------------------------------*/
	public Usuario login(Usuario usuario) throws DataException, LoginException {
		Connection conn = null;
		Usuario usuarioL;
		try {
			conn = this.conn.getNewConnection();
			usuarioL = dao.login(conn, usuario);
			if (!usuario.getContraseña().equals(usuarioL.getContraseña())) {
				throw new LoginException("El Usuario y la Contraseña no coinciden");
			}
		} catch (SQLException e) {
			throw new DataException("No se ha encontrado el Usuario", e);
		} finally {
			cerrarConexion(conn);
		}
		return usuarioL;
	}

	/*-----------------------------------Modificar-----------------------------------*/
	public void actualizarUsuario(Usuario usuario) throws DataException {
		Connection conn = null;
		try {
			conn = this.conn.getNewConnection();
			conn.setAutoCommit(false);
			dao.actualizarUsuario(conn, usuario);
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new DataException("Se ha producido un error al modificar los datos", e);
		} finally {
			cerrarConexion(conn);
		}
	}

	/*-----------------------------------Registrar-----------------------------------*/
	public void crearUsuario(Usuario usuario) throws DataException {
		Connection conn = null;
		try {
			conn = this.conn.getNewConnection();
			conn.setAutoCommit(false);
			dao.crearUsuario(conn, usuario);
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new DataException("Se ha producido un error al crear el usuario", e);
		} finally {
			cerrarConexion(conn);
		}
	}

	/*-----------------------------------Eliminar-----------------------------------*/
	public void borrarUsuario(Usuario usuario) throws DataException {
		Connection conn = null;
		try {
			conn = this.conn.getNewConnection();
			conn.setAutoCommit(false);
			dao.eliminarUsuario(conn, usuario);
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			throw new DataException("Se ha producido un error al eliminar los datos", e);
		} finally {
			cerrarConexion(conn);
		}
	}

	private void cerrarConexion(Connection conn) throws DataException {
		try {
			conn.close();
		} catch (SQLException e) {
			throw new DataException(
					"no se ha podido cerrar la conexion porque no habia ninguna abierta o se ha producido un error", e);
		}
	}

}
