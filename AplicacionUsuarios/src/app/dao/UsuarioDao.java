package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import app.modelo.Usuario;

public class UsuarioDao {

	/*-------------------------------Insert-------------------------------*/
	public void crearUsuario(Connection conn, Usuario usuario) throws SQLException {
		PreparedStatement stmt = null;
		try {
			String query = "INSERT INTO usuarios(usuario, password, nombre, apellidos) VALUES(?, ?, ?, ?)";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, usuario.getNombre());
			stmt.setString(2, usuario.getContraseña());
			stmt.setString(3, usuario.getNombre());
			stmt.setString(4, usuario.getApellido());
			stmt.execute();
		} finally {
			cerrarStatement(stmt);
		}
	}

	/*-------------------------------Update-------------------------------*/
	public void actualizarUsuario(Connection conn, Usuario usuario) throws SQLException {
		PreparedStatement stmt = null;
		try {
			String query = "UPDATE usuarios SET usuario = ?, password = ?, nombre = ?, apellidos = ? WHERE id = ?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, usuario.getUsuario());
			stmt.setString(2, usuario.getContraseña());
			stmt.setString(3, usuario.getNombre());
			stmt.setString(4, usuario.getApellido());
			stmt.setInt(5, usuario.getId());
			stmt.execute();
		} finally {
			cerrarStatement(stmt);
		}
	}

	/*-------------------------------Select-------------------------------*/
	public Usuario login(Connection conn, Usuario usuario) throws SQLException {
		Usuario us = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement("SELECT * FROM usuarios WHERE usuario = ?");
			stmt.setString(1, usuario.getUsuario());
			rs = stmt.executeQuery();
			us = añadirUsuario(rs);
		} finally {
			cerrarStatement(stmt, rs);
		}
		return us;
	}

	/*-------------------------------Delete-------------------------------*/
	public void eliminarUsuario(Connection conn, Usuario usuario) throws SQLException {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("DELETE FROM usuarios WHERE id = ?");
			stmt.setInt(1, usuario.getId());
			stmt.execute();
		} finally {
			cerrarStatement(stmt);
		}
	}

	/*-------------------------------Metodo extra crear usuario-------------------------------*/
	private Usuario añadirUsuario(ResultSet rs) throws SQLException {
		rs.next();
		Usuario usuario = new Usuario();
		usuario.setId(rs.getInt("id"));
		usuario.setUsuario(rs.getString("usuario"));
		usuario.setContraseña(rs.getString("password"));
		usuario.setNombre(rs.getString("nombre"));
		usuario.setApellido(rs.getString("apellidos"));
		return usuario;
	}

	/*-------------------------------Cerrar statement y resulset-------------------------------*/
	private void cerrarStatement(PreparedStatement stmt, ResultSet rs) throws SQLException {
		try {
			if (stmt != null) {
				stmt.close();
			}
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			throw new SQLException("no se puede cerrar el statement o el resulset porque no hay ninguo abierto");
		}
	}

	private void cerrarStatement(PreparedStatement stmt) throws SQLException {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new SQLException("no se puede cerrar el statement porque no hay ninguo abierto");
		}
	}
}
