package app.gui;

import javax.swing.JTextField;

import app.Controlador;

import javax.swing.JLabel;
import javax.swing.JButton;

public class DatosView extends AbstractView {

	private static final long serialVersionUID = -7808990705857028038L;
	private JTextField tfUsuario;
	private JTextField tfNombre;
	private JTextField tfApellidos;
	private Controlador controlador;

	public DatosView(Controlador controlador) {
		this.controlador = controlador;
		setLayout(null);
		
		tfUsuario = new JTextField();
		tfUsuario.setBounds(167, 40, 209, 20);
		add(tfUsuario);
		tfUsuario.setColumns(10);
		tfUsuario.setEditable(false);
		
		tfNombre = new JTextField();
		tfNombre.setColumns(10);
		tfNombre.setBounds(167, 78, 209, 20);
		add(tfNombre);
		tfNombre.setEditable(false);
		
		tfApellidos = new JTextField();
		tfApellidos.setColumns(10);
		tfApellidos.setBounds(167, 118, 209, 20);
		add(tfApellidos);
		tfApellidos.setEditable(false);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(38, 43, 76, 14);
		add(lblUsername);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(38, 81, 76, 14);
		add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(38, 118, 76, 14);
		add(lblApellidos);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(38, 176, 89, 23);
		btnSalir.setActionCommand("inicio");
		add(btnSalir);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(148, 176, 89, 23);
		btnModificar.setActionCommand("modificar");
		add(btnModificar);
		
		JButton btnEliminar = new JButton("Eliminar Usuario");
		btnEliminar.setBounds(263, 176, 142, 23);
		btnEliminar.setActionCommand("eliminar");
		add(btnEliminar);

		btnEliminar.addActionListener(this.controlador);
		btnSalir.addActionListener(this.controlador);
		btnModificar.addActionListener(this.controlador);
	}

	@Override
	public void initialize() {		
	}

	public JTextField gettfUsuario() {
		return tfUsuario;
	}

	public void settfUsuario(String usuario) {
		this.tfUsuario.setText(usuario); 
	}

	public JTextField gettfNombre() {
		return tfNombre;
	}

	public void setTexttfNombre(String nombre) {
		this.tfNombre.setText(nombre); 
	}

	public JTextField gettfApellidos() {
		return tfApellidos;
	}

	public void settfApellidos(String apellidos) {
		this.tfApellidos.setText(apellidos); 
	}
	
}
