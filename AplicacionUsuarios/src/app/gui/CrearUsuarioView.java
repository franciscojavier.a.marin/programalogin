package app.gui;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import app.Controlador;

public class CrearUsuarioView extends AbstractView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8649956286948685104L;
	private Controlador controlador;
	private JTextField tfUsername;
	private JPasswordField tfPassword;
	private JTextField tfNombre;
	private JTextField tfApellidos;

	public CrearUsuarioView(Controlador controlador) {
		this.controlador = controlador;
		setLayout(null);
		tfUsername = new JTextField();
		tfUsername.setBounds(158, 63, 187, 20);
		add(tfUsername);
		tfUsername.setColumns(10);

		tfPassword = new JPasswordField();
		tfPassword.setBounds(158, 94, 187, 20);
		add(tfPassword);
		tfPassword.setColumns(10);
		tfPassword.setEchoChar('*');
		
		tfNombre = new JTextField();
		tfNombre.setBounds(158, 125, 187, 20);
		add(tfNombre);
		tfNombre.setColumns(10);

		tfApellidos = new JTextField();
		tfApellidos.setBounds(158, 156, 187, 20);
		add(tfApellidos);
		tfApellidos.setColumns(10);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(42, 66, 80, 14);
		add(lblUsername);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(42, 97, 80, 14);
		add(lblPassword);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(42, 128, 80, 14);
		add(lblNombre);

		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(42, 159, 80, 14);
		add(lblApellidos);

		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(this.controlador);
		btnGuardar.setBounds(173, 200, 89, 23);
		btnGuardar.setActionCommand("crearUsu");
		add(btnGuardar);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(this.controlador);
		btnVolver.setBounds(272, 200, 89, 23);
		btnVolver.setActionCommand("inicio");
		add(btnVolver);
		
		JButton btnVer = new JButton("Ver");
		btnVer.setBounds(351, 93, 60, 23);
		add(btnVer);
		btnVer.setActionCommand("verC");
		btnVer.addActionListener(this.controlador);

	}

	public String gettfUsuario() {
		return tfUsername.getText();
	}

	public void settfUsuario(String usuario) {
		this.tfUsername.setText(usuario); 
	}

	public String gettfNombre() {
		return tfNombre.getText();
	}

	public void setTexttfNombre(String nombre) {
		this.tfNombre.setText(nombre); 
	}

	public String gettfApellidos() {
		return tfApellidos.getText();
	}

	public void settfApellidos(String apellidos) {
		this.tfApellidos.setText(apellidos); 
	}
	public String gettfPassword() {
		return new String(tfPassword.getPassword());
	}

	public void settfPassword(String Password) {
		this.tfPassword.setText(Password); 
	}
	
	public void verPassword() {
		if (tfPassword.getEchoChar() == (char)0) {
			tfPassword.setEchoChar('*');
		}else {
		tfPassword.setEchoChar((char)0);
		}
	}
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

}
