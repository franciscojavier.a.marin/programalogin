package app.gui;

import javax.swing.JTextField;

import app.Controlador;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class LoginView extends AbstractView {
	private static final long serialVersionUID = -2259264505318632836L;
	private Controlador controlador;
	private JTextField tfUsuario;
	private JPasswordField tfPassword;
	

	public LoginView(Controlador controlador) {
		this.controlador = controlador;
		setLayout(null);

		tfUsuario = new JTextField();
		tfUsuario.setBounds(100, 69, 86, 20);
		add(tfUsuario);
		tfUsuario.setColumns(10);

		tfPassword = new JPasswordField();
		tfPassword.setBounds(100, 123, 86, 20);
		add(tfPassword);
		tfPassword.setColumns(10);

		JLabel lblPassword = new JLabel("Contraseña");
		lblPassword.setBounds(30, 126, 73, 14);
		add(lblPassword);

		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(30, 72, 46, 14);
		add(lblUsuario);

		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(295, 144, 103, 23);
		btnEntrar.setActionCommand("entrar");
		add(btnEntrar);

		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(295, 178, 103, 23);
		btnSalir.setActionCommand("salir");
		add(btnSalir);
		
		JButton btncrear = new JButton("Registrarse");
		btncrear.setBounds(295, 212, 103, 23);
		btncrear.setActionCommand("crear");
		add(btncrear);
		
		JButton btnVer = new JButton("Ver");
		btnVer.setBounds(201, 122, 55, 23);
		add(btnVer);
		btnVer.setActionCommand("ver");
		
		tfUsuario.addKeyListener(controlador);
		tfPassword.addKeyListener(controlador);
		tfPassword.setEchoChar('*');
		btncrear.addActionListener(this.controlador);
		btnSalir.addActionListener(this.controlador);
		btnEntrar.addActionListener(this.controlador);
		btnVer.addActionListener(this.controlador);
	}

	public String gettfUsuario() {
		return tfUsuario.getText();
	}

	public void settfUsuario(String usuario) {
		this.tfUsuario.setText(usuario); 
	}

	public String gettfPassword() {
		return new String(tfPassword.getPassword());
	}

	public void settfPassword(String Password) {
		this.tfPassword.setText(Password); 
	}
	
	public void verPassword() {
		if (tfPassword.getEchoChar() == (char)0) {
			tfPassword.setEchoChar('*');
		}else {
		tfPassword.setEchoChar((char)0);
		}
	}
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}
}
