package app.gui;

import javax.swing.JPanel;

import app.Controlador;

public abstract class AbstractView extends JPanel {

	private static final long serialVersionUID = 6636161156327632113L;

	public Controlador appController;

	public abstract void initialize();

}
