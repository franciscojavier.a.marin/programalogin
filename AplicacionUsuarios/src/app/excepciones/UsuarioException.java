package app.excepciones;

public class UsuarioException extends Exception {

	private static final long serialVersionUID = -646046308253564031L;

	public UsuarioException() {
	}
	
	public UsuarioException(String mensaje) {
	super(mensaje);	
	}
	
	public UsuarioException(Throwable cause) {
		super(cause);
	}

	public UsuarioException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public UsuarioException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
