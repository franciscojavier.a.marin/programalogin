package app.excepciones;

public class DataException extends Exception {

	private static final long serialVersionUID = -1873620310216151944L;

	public DataException() {
	}

	public DataException(String mensaje) {
		super(mensaje);
	}

	public DataException(Throwable cause) {
		super(cause);
	}

	public DataException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
