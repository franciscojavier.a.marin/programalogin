package app;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import app.excepciones.DataException;
import app.excepciones.LoginException;
import app.gui.AbstractView;
import app.gui.CrearUsuarioView;
import app.gui.DatosView;
import app.gui.LoginView;
import app.gui.ActualizarView;
import app.modelo.Usuario;
import app.service.UsuarioService;

public class Controlador implements ActionListener, KeyListener {

	private JFrame frame;
	private LoginView loginView;
	private DatosView datosView;
	private ActualizarView actualizarView;
	private CrearUsuarioView crearUsuarioView;
	private static UsuarioService service;
	private Usuario usuario;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Controlador window = new Controlador();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Controlador() {
		service = new UsuarioService();
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginView = new LoginView(this);
		datosView = new DatosView(this);
		actualizarView = new ActualizarView(this);
		crearUsuarioView = new CrearUsuarioView(this);
		frame.setContentPane(loginView);
		frame.setTitle("Login");
	}

	/*-------------------conexion con service-------------------*/

	public Usuario login(Usuario usuario) throws DataException, LoginException {
		return service.login(usuario);
	}

	public void actializarUsuario(Usuario usuario) throws DataException {
		service.actualizarUsuario(usuario);
		this.usuario = usuario;
	}

	public void crearUsuario(Usuario usuario) throws DataException, LoginException {
		service.crearUsuario(usuario);
		this.usuario = service.login(usuario);
	}

	public Boolean eliminarUsuario() throws DataException {
		Integer i = JOptionPane.showConfirmDialog(frame, "¿esta seguro de borrar esta cuenta?", "Alerta",
				JOptionPane.WARNING_MESSAGE);
		if (i == 0) {
			service.borrarUsuario(usuario);
			return true;
		}
		return false;
	}

	/*----------------------------------------metodos-opciones-aplicacion----------------------------------------*/

	/*---------------login---------------*/
	private void hacerLogin() {
		Usuario usuario = new Usuario();
		usuario.setUsuario(loginView.gettfUsuario());
		usuario.setContraseña(loginView.gettfPassword());
		try {
			this.usuario = login(usuario);
		} catch (DataException | LoginException e1) {
			mostrarPopUp(e1.getMessage());
			e1.printStackTrace();
		}
	}

	/*---------------modificar usuario---------------*/
	private void actualizarUsuario() {
		Usuario usuario = new Usuario();
		usuario.setId(this.usuario.getId());
		if (actualizarView.gettfNombre().equals("")) {
			usuario.setNombre(this.usuario.getNombre());
		} else {
			usuario.setNombre(actualizarView.gettfNombre());
		}
		
		if (actualizarView.gettfApellidos().equals("")) {
			usuario.setApellido(this.usuario.getApellido());
		} else {
			usuario.setApellido(actualizarView.gettfApellidos());
		}
		
		if (actualizarView.gettfUsuario().equals("")) {
			usuario.setUsuario(this.usuario.getUsuario());
		} else {
			usuario.setUsuario(actualizarView.gettfUsuario());
		}
		
		if (actualizarView.gettfPassword().equals("")) {
			usuario.setContraseña(this.usuario.getContraseña());
		} else {
			usuario.setContraseña(actualizarView.gettfPassword());
		}
		
		try {
			actializarUsuario(usuario);
		} catch (DataException e1) {
			mostrarPopUp(e1.getMessage() + "\n(datos duplicados)");
			e1.printStackTrace();
		}
	}

	/*---------------borrar usuario---------------*/
	private void eliminar(Boolean b) {
		try {
			b = eliminarUsuario();
		} catch (DataException e1) {
			mostrarPopUp(e1.getMessage());
			e1.printStackTrace();
		}
	}

	private void crear() {
		Usuario usuario = new Usuario();
		usuario.setNombre(crearUsuarioView.gettfNombre());
		usuario.setApellido(crearUsuarioView.gettfApellidos());
		usuario.setUsuario(crearUsuarioView.gettfUsuario());
		usuario.setContraseña(crearUsuarioView.gettfPassword());
		try {
			crearUsuario(usuario);
		} catch (DataException | LoginException e1) {
			mostrarPopUp(e1.getMessage());
			e1.printStackTrace();
		}
	}

	/*-----------------------------------------------------------------------------------------------------------*/

	/*-------------------------------cambio de vista-------------------------------*/

	private void cambiarALogin() {
		cambiarView(loginView);
		frame.setTitle("Login");
	}

	private void cambiarARegistrarse() {
		cambiarView(crearUsuarioView);
		frame.setTitle("Registrarse");
	}

	private void cambiarADatos() {
		cambiarView(datosView);
		frame.setTitle("Datos personales");
	}

	private void cambiarAModificar() {
		cambiarView(actualizarView);
		frame.setTitle("Modificar datos personales");
	}

	public void cambiarView(AbstractView view) {
		if (view.equals(datosView)) {
			datosView.settfUsuario(this.usuario.getUsuario());
			datosView.setTexttfNombre(this.usuario.getNombre());
			datosView.settfApellidos(this.usuario.getApellido());
		}
		frame.setContentPane(view);
		view.initialize();
		frame.revalidate();
	}

	/*-----------------------------------------popup-----------------------------------------*/

	private void mostrarPopUp(String mensaje) {
		JOptionPane.showMessageDialog(frame, mensaje, "Error", JOptionPane.ERROR_MESSAGE /* 0 */);
	}

	/*--------------------------------salir de la app--------------------------------*/
	public void salir() {
		System.exit(0);
	}

	/*-----------------------------------------listeners-----------------------------------------*/

	/*--------------------------------------action listener--------------------------------------*/

	@Override
	public void actionPerformed(ActionEvent e) {
		/*---------------login---------------*/
		if (e.getActionCommand().equals("entrar")) {
			hacerLogin();
			cambiarADatos();
		}
		/*---------------Cerrar App---------------*/
		else if (e.getActionCommand().equals("salir")) {
			salir();
		}
		/*---------------abrir Modificar Usuario---------------*/
		else if (e.getActionCommand().equals("modificar")) {
			cambiarAModificar();
		}
		/*---------------volver a login---------------*/
		else if (e.getActionCommand().equals("inicio")) {
			cambiarALogin();
		}
		/*---------------volver a datos---------------*/
		else if (e.getActionCommand().equals("volver")) {
			cambiarADatos();
		}
		/*---------------modificar usuario---------------*/
		else if (e.getActionCommand().equals("guardar")) {
			actualizarUsuario();
			cambiarADatos();
		}
		/*---------------borrar usuario---------------*/
		else if (e.getActionCommand().equals("eliminar")) {
			Boolean bl = false;
			eliminar(bl);
			if (bl) {
				cambiarALogin();
			}
		}
		/*---------------abrir crear usuario---------------*/
		else if (e.getActionCommand().equals("crear")) {
			cambiarARegistrarse();
		}
		/*---------------crear usuario---------------*/
		else if (e.getActionCommand().equals("crearUsu")) {
			crear();
			cambiarADatos();
		}
		/*---------------ver contraseña---------------*/
		else if (e.getActionCommand().equals("ver")) {
			loginView.verPassword();
		} else if (e.getActionCommand().equals("verA")) {
			actualizarView.verPassword();
		} else if (e.getActionCommand().equals("verC")) {
			crearUsuarioView.verPassword();
		}
	}

	/*-----------------------------------Key listener---------------------------------------*/

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			((JComponent) e.getSource()).transferFocus();
		} else if (e.getKeyCode() == KeyEvent.VK_UP) {
			((JComponent) e.getSource()).transferFocusBackward();
		} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			((JComponent) e.getSource()).transferFocus();
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
